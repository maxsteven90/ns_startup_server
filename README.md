# ns_Startup Chat Server v0.0.02 for ns_Startup

[![Vimeo](https://i.vimeocdn.com/video/823525142.jpg)](https://player.vimeo.com/video/367236737 "Vimeo")(Click to start Vimeo teaser)

Simple Chatserver for ns_Startup:

![Inline image](docs/ns_Startup_Server_Screener.jpg)

https://gitlab.com/e_noni/ns_startup

- Windows only right now
- you need a proper Python 2.7 and PyQT4 installation (other libraries as well), or you build a your own virtual environment. Easy with PyCharm.
- check the init values in the ServerThread class
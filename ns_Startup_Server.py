version = "v0.0.02"
## Niclas Schlapmann - Freelance 3D Generalist
## www.enoni.de
## hello@enoni.de
## ns_Startup Chat Server
## 08.10.2019
##############################################################################################################
## EDIT FROM HERE ##


import os
from time import *
from datetime import datetime
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import QtGui, QtCore, uic
import sys
import time
import Queue
import threading
from threading import Thread
import socket
import getpass
import json
import win32serviceutil
import win32service
import win32event
import servicemanager
import resource

##############################################################################################################
lt = localtime()
jahr, monat, tag = lt[0:3]
ns_date = str(jahr)[2:4]+str(monat).zfill(2)+str(tag).zfill(2)
user = getpass.getuser()
machine = socket.gethostname()
##################################### LOOKUP PATHES ##########################################################

class Controller:
    def __init__(self, serverThread):
        app = QApplication(sys.argv)
        self.gui = MainWindow(self)
        self.ServerThread = serverThread
        
        ## SIGNALS ##
        self.gui.connect(self.ServerThread, SIGNAL("writeList(QString)"), self.writeList)

        ## BUTONS ##
        self.gui.connect(self.gui.pushButton_clear_log, QtCore.SIGNAL('clicked()'), self.clearList)

        self.clearList()
        serverThread.start()
        self.gui.show()
        app.exec_()

    def writeList(self, text):
        tmp = self.gui.textEdit_debug_log.toPlainText()
        self.gui.textEdit_debug_log.setText(tmp + "\n" + text)

    def clearList(self):
        self.gui.textEdit_debug_log.setText("----------------------------------------------------------------------------------------------------------\n" + "ns_Startup - Chat Server " + version + "\n----------------------------------------------------------------------------------------------------------")



class ClientThreadRead(Thread):
    def __init__(self, sockClient, ip, port, serverThread, users_online):
        Thread.__init__(self)
        self.sockClient = sockClient
        self.ip = ip
        self.port = port
        self.serverThread = serverThread
        self.DATA_RECIEVED = ""
        self.users_online = users_online
        self.user = ""


    def dataToSend(self, socketToSend, typeString, dataString, ipString):
        DATA_TO_SEND = json.dumps({"arg": [typeString, dataString, ipString]})
        socketToSend.send(DATA_TO_SEND.encode('utf-8'))
    
    def dataRecieved(self, recievingSocket, indexString):
        try:
            DATA_RECIEVED = (json.loads(recievingSocket.recv(self.serverThread.BUFFER_SIZE).decode('utf-8'))).get(indexString)
            return DATA_RECIEVED
        except:
            self.sockClient.close()
            print("ClientThreadRead exit. (dataRecieved)")
            sys.exit()


    def run(self):
        try:
            while True:
                self.dataToSend(self.sockClient, "c" , serverThread.TIME_OUT, self.ip)
                while True:
                    self.DATA_RECIEVED = self.dataRecieved(self.sockClient, "arg")

                    if self.DATA_RECIEVED[0] == "m":
                        serverThread.lock.acquire()
                        for q in serverThread.sendqueues.values():
                            q.put([self.DATA_RECIEVED[1], self.DATA_RECIEVED[2]])
                        serverThread.lock.release()
                        self.dataToSend(self.sockClient, "c", "_broadcasted_", socket.gethostbyname(socket.gethostname()))
                    
                    if  self.DATA_RECIEVED[0] == "u":
                        self.user = self.DATA_RECIEVED[1]
                        if self.DATA_RECIEVED[1] not in self.users_online:
                            self.users_online.append(self.DATA_RECIEVED[1])
                        online_list = "Users online: "
                        for i in self.users_online:
                            online_list = online_list + i + ", "
                        online_list[:-1]
                        self.dataToSend(self.sockClient, "m", online_list, socket.gethostbyname(socket.gethostname()))
                    
                    if self.DATA_RECIEVED[0] == "c":
                        if self.DATA_RECIEVED[1] == "_exit_":
                            fd = self.sockClient.fileno()
                            serverThread.lock.acquire()
                            del self.serverThread.sendqueues[fd]
                            self.dataToSend(self.sockClient, "c", "_exit_ok_", socket.gethostbyname(socket.gethostname()))
                            self.serverThread.emit(SIGNAL("writeList(QString)"), datetime.now().strftime("%H:%M:%S") + " > " + "Remove Client: " + str(self.ip) + ":" + str(self.port))
                            serverThread.lock.release()
                            self.users_online.remove(self.user)
                            break
                break
            print("ClientThreadRead exit.")
            self.users_online.remove(self.user)
            self.sockClient.close()
            sys.exit()
        except socket.error:
            print("ClientThreadRead exit. (socket.error)")
            self.users_online.remove(self.user)
            self.sockClient.close()
            sys.exit()
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            self.serverThread.emit(SIGNAL("writeList(QString)"), datetime.now().strftime("%H:%M:%S") + " > " + "No Connection to Client (ClientThreadRead): " + str(self.ip)+ ":" + str(self.port))
            self.users_online.remove(self.user)
            self.sockClient.close()
            sys.exit()


class ClientThreadSend(Thread):
    def __init__(self, sockClient, ip, port, serverThread, users_online):
        Thread.__init__(self)
        self.sockClient = sockClient
        self.ip = ip
        self.port = port
        self.serverThread = serverThread
        self.DATA_RECIEVED = ""
        self.users_online = users_online
        self.user = ""

    def dataToSend(self, socketToSend, typeString, dataString, ipString):
        DATA_TO_SEND = json.dumps({"arg": [typeString, dataString, ipString]})
        socketToSend.send(DATA_TO_SEND.encode('utf-8'))

    def dataRecieved(self, recievingSocket, indexString):
        try:
            DATA_RECIEVED = (json.loads(recievingSocket.recv(self.serverThread.BUFFER_SIZE).decode('utf-8'))).get(indexString)
            return DATA_RECIEVED
        except:
            self.users_online.remove(self.user)
            self.sockClient.close()
            print("ClientThreadSend exit. (dataRecieved)")
            sys.exit()

    def run(self):
        self.serverThread.sockRead.listen(1)
        sockClient2, addr = self.serverThread.sockRead.accept()
        self.dataToSend(sockClient2, "m", "## ns_Startup - Chat Server " + version + " @ " + machine + " ##", socket.gethostbyname(socket.gethostname()))
        chat = ""
        while True:
            try:
                chat = self.serverThread.sendqueues[self.sockClient.fileno()].get(False)
                self.dataToSend(sockClient2, "m", chat[0], chat[1])
                time.sleep(2)
            except Queue.Empty:
                chat = ""
                time.sleep(2)
            except socket.error:
                print("ClientThreadSend exit. (socket.error)")
                sockClient2.close()
                self.users_online.remove(self.user)
                self.sockClient.close()
                sys.exit()
            except Exception as e:
                 print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
                 self.serverThread.emit(SIGNAL("writeList(QString)"), datetime.now().strftime("%H:%M:%S") + " > " + "No Connection to Client (ClientThreadSend): " + str(self.ip) + ":" + str(self.port))
                 self.users_online.remove(self.user)
                 self.sockClient.close()
                 sys.exit()


class ServerThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.users_online = []
        self.TCP_IP = machine
        self.TCP_PORT = 666
        self.TCP_PORT2 = self.TCP_PORT + 1
        self.BUFFER_SIZE = 4096
        self.TIME_OUT = 10
        self.BLOCK_TIME = 60.0
        self.threadStack = []
        self.lock = threading.Lock()
        self.sendqueues = {}
        self.sockSend = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sockSend.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sockSend.bind(('', self.TCP_PORT))
        self.sockRead = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sockRead.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sockRead.bind(('', self.TCP_PORT2))
        

    def run(self):
        self.emit(SIGNAL("writeList(QString)"), datetime.now().strftime("%H:%M:%S") + " > " + "Server is Running: " + self.TCP_IP + ":" + str(self.TCP_PORT))
        while True:
            self.sockSend.listen(6)
            sockClient, (ip, port) = self.sockSend.accept()
            self.lock.acquire()
            self.sendqueues[sockClient.fileno()] = Queue.Queue()
            self.lock.release()
            self.emit(SIGNAL("writeList(QString)"), datetime.now().strftime("%H:%M:%S") + " > " + "Add Client: " + str(ip) + ":" + str(port))

            sendThread = ClientThreadSend(sockClient, ip, port, self, self.users_online)
            sendThread.daemon = True
            sendThread.start()
            readThread = ClientThreadRead(sockClient, ip, port, self, self.users_online)
            readThread.daemon = True

            readThread.start()
            self.threadStack.append(sendThread)
            self.threadStack.append(readThread)

            tmp = []

            for i, t in enumerate(self.threadStack):
                print(t.isAlive())
                if t.isAlive():
                    tmp.append(t)

            self.threadStack = tmp

            print(len(self.threadStack))
            print(self.users_online)

        for t in self.threadStack:
            t.join()



class MainWindow(QMainWindow):
    def __init__(self, controller):
        QMainWindow.__init__(self)
        self.controller = controller
        uic.loadUi(sys.path[0] + os.sep + "UI" + os.sep + "ns_Startup_Server.ui", self)

    def closeEvent(self, event):
        try:
            reply = QtGui.QMessageBox.question(self, "Warning", "Are you sure, quit the ns_Startup Chat Server?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                event.accept()
            else:
                event.ignore()
        except:
            pass


if __name__ == "__main__":
    serverThread = ServerThread()
    ns_Startup_Host = Controller(serverThread)


